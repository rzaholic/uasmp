import 'package:flutter/material.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';

class HomePage extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.home),
        title: Text('Berita Terbaru'),
        actions: <Widget>[
          Icon(Icons.search),
        ],
        elevation: 0,
        backgroundColor: Color(0xFF5cc3fd),
        centerTitle: true,
      ),
    );
  }
}

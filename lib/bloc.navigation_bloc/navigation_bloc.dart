import 'package:bloc/bloc.dart';
import 'package:informasi_covid/pages/home_page.dart';
import 'package:informasi_covid/pages/profile_page.dart';
import 'package:informasi_covid/pages/setting.dart';





enum NavigationEvents {
  HomePageClickedEvent,
  ProfilePageClickedEvent,
  SettingsClickedEvent,
}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => HomePage();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield HomePage();
        break;
      case NavigationEvents.ProfilePageClickedEvent:
        yield ProfilePage();
        break;
      case NavigationEvents.SettingsClickedEvent:
        yield SettingsPage();
        break;
      // case NavigationEvents.MyOrdersClickedEvent:
      //   yield MyRentsPage();
      //   break;
      
    }
  }
}
